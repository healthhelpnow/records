import CoreData
import XCTest

final class PartyTests: BaseTests {
    
    func testDefaultValues() {
        let party = Party(context: container.viewContext)
        let message = "Default value missing"
        XCTAssert(party.type_ == .school, message)
    }
}
