<p align="center">
    <img src="Logo.png" width="480" max-width="90%" alt="Records" />
</p>

<p align="center">
    <a href="https://travis-ci.org/rob-nash/Records">
        <img src="https://travis-ci.org/rob-nash/Records.svg?branch=master" alt="Build"/>
    </a>
    <a href="https://img.shields.io/badge/carthage-compatible-brightgreen.svg">
        <img src="https://img.shields.io/badge/carthage-compatible-brightgreen.svg" alt="Carthage"/>
    </a>
    <a href="https://codebeat.co/projects/github-com-rob-nash-records-master">
    	  <img alt="codebeat badge" src="https://codebeat.co/badges/94dfa117-7d48-451d-bff9-81117efe5032"/>
    </a>
    <a href="https://twitter.com/nashytitz">
        <img src="https://img.shields.io/badge/contact-@nashytitz-blue.svg?style=flat" alt="Twitter: @nashytitz"/>
    </a>
</p>

A very light-weight **CoreData** wrapper. A lot of boiler plate is required so we recommend using [Soucery](https://github.com/krzysztofzablocki/Sourcery) with this. The full [installation guide](https://github.com/rob-nash/Records/wiki/Installation) for Records + Sourcery only takes a few minutes to complete.

<p align="center">
<a href="https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/CoreData/KeyConcepts.html">
<img src="https://i.imgur.com/WRlhnlK.png" alt="CoreData" />
</a>
</p>

## Fetch

```swift
do {
  let performances: [Performance] = try Performance.Query(group: .solo).all(in: context)
  performances.forEach { (performance) in
    context.delete(performance)
  }
  try context.save()
} catch {
  // Errors from the CoreData layer such as 'model not found' etc
}
```

```swift
do {
  let lower = date.oneDayEarlier
  let upper = date.oneDayLater
  let performer: Performer? = try Performer.Query(dob: lower...upper, firstName: "Stacey").first(in: context)
} catch {
  // Errors from the CoreData layer such as 'model not found' etc
}
```

[More details](https://github.com/rob-nash/Records/wiki/Fetching)

## Create

```swift
struct Information {
    let name: String
    let phone: String
    let email: String
    let type: String

    // implement protocol named `Recordable` here ~ 2 minutes
}

let info = Information(name: "DanceSchool", phone: "01234567891", email: "dance@school.com", type: "School")

do {
    let record: Party = try info.record(in: context)
} catch {
    // Errors from the CoreData layer such as 'model not found' etc
}
```

When using the protocol named `Recordable` we ensure the following is performed when we call `record(in: )`.

1. If a record does not exist that matches this data, it is created.
2. If a record does exist that matches this data, it is retrieved.

[More details](https://github.com/rob-nash/Records/wiki/Create)

## Observe

For each entity you observe, create a unique tableView or collectionView type. Then have each view conform to the `FetchedResultsControllerDelegate`. 

```swift
import UIKit

final class PerformersCollectionViewController: UIViewController {
    
    let performersController = PerformerController<PerformersCollectionView>()
    
    private lazy var collectionViewHandler = PerformerCollectionViewHandler(performersController)
    
    @IBOutlet weak var collectionView: PerformersCollectionView! {
        didSet {
            collectionView.dataSource = collectionViewHandler
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performersController.delegate = collectionView
    }
}
```

[More details](https://github.com/rob-nash/Records/wiki/Observe)

## Installation

1. Run the [Carthage](https://github.com/Carthage/Carthage#installing-carthage) command `carthage update`.
2. Embed Binary.
3. Install [Sourcery](https://github.com/krzysztofzablocki/Sourcery#installation).
4. Adjust NSManagedObject subclasses `~2 minute job`.

[See Guide](https://github.com/rob-nash/Records/wiki/Installation)

## Demo

1. Run the [Carthage](https://github.com/Carthage/Carthage#installing-carthage) command `carthage bootstrap --platform ios`.
2. Install [Sourcery](https://github.com/krzysztofzablocki/Sourcery#installation).
3. Run the Xcode scheme named `Performances`.

## Footnotes

See [footnotes](https://github.com/rob-nash/Records/wiki/Footnotes) for some handy tips.
