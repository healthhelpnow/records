//
//  EmptyViewController.swift
//  Performances
//
//  Created by Robert Nash on 16/10/2018.
//  Copyright © 2018 Robert Nash. All rights reserved.
//

import UIKit
import Sheet
import Rotary

final class EmptyViewController: UIViewController {
  
  private let sheetManager = Sheet(animation: .fade, widthInset: 30, bottomInset: 30)
  
  private lazy var selectedDemo = options.first.require()
  
  private let options = (1...4).map(Demo.init)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    sheetManager.chromeTapped = { [unowned self] in
      self.dismiss(animated: true)
    }
    NotificationCenter.default.addObserver(forName: .dismiss, object: nil, queue: nil) { [weak self] _ in
      self?.dismiss(animated: true)
    }
    title = "Records"
    let optionSelectorWheelView: OptionSelectorWheelView = OptionSelectorWheelView.load()
    optionSelectorWheelView.wheelControl.rotationEnded = { [unowned self] demo in
      self.selectedDemo = demo
    }
    optionSelectorWheelView.wheelControl.layout(options)
    view.addSubview(optionSelectorWheelView)
    let constraint = optionSelectorWheelView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor)
    constraint.priority = .required
    NSLayoutConstraint.activate([
      constraint,
      optionSelectorWheelView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      optionSelectorWheelView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      optionSelectorWheelView.heightAnchor.constraint(equalToConstant: 350)
      ])
  }
  
  @IBAction func goButtonPressed(_ sender: UIBarButtonItem) {
    let identifier = String(selectedDemo.index)
    performSegue(withIdentifier: identifier, sender: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let storyboard = UIStoryboard(name: "RecordSheet", bundle: nil)
    let controller = storyboard.instantiateInitialViewController().require()
    self.sheetManager.show(controller, above: self)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}
