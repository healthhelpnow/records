//
//  MessageSheet.swift
//  Performances
//
//  Created by Robert Nash on 17/10/2018.
//  Copyright © 2018 Robert Nash. All rights reserved.
//

import UIKit

class MessageSheet: UIViewController {
  
  @IBOutlet weak var imageView: UIImageView!
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    view.clipsToBounds = true
    view.layer.cornerRadius = view.bounds.width * 0.1
    view.layer.borderWidth = 2
    view.layer.borderColor = UIColor.white.cgColor
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    let tap = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognised(_:)))
//    view.addGestureRecognizer(tap)
  }
  
  @objc
  private func tapGestureRecognised(_ gesture: UITapGestureRecognizer) {
    NotificationCenter.default.post(name: .dismiss, object: nil)
  }
}
