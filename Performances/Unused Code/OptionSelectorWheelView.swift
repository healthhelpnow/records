import UIKit
import Rotary

struct Demo: WheelOption {
  let index: Int
  var name: String? {
    switch index {
    case 1:
      return "Observer"
    case 2:
      return "Filter"
    case 3:
      return "Query"
    case 4:
      return "Aggregate"
    default:
      return nil
    }
  }
  static func ==(lhs: Demo, rhs: Demo) -> Bool {
    return lhs.index == rhs.index
  }
}

extension Demo: CustomStringConvertible {
  var description: String {
    return name.require()
  }
}

class OptionSelectorWheelView: UIView {
  static func load() -> OptionSelectorWheelView {
    let wheel = Bundle(for: OptionSelectorWheelView.self).loadNibNamed("OptionSelectorWheelView", owner: nil, options: nil)!.first as! OptionSelectorWheelView
    wheel.translatesAutoresizingMaskIntoConstraints = false
    return wheel
  }
  @IBOutlet private weak var selectionBar: UIView! {
    didSet {
      selectionBar.backgroundColor = pink
    }
  }
  let wheelControl = RotaryWheelControl<Demo>()
  @IBOutlet private weak var wheelContainer: UIView! {
    didSet {
      wheelControl.translatesAutoresizingMaskIntoConstraints = false
      wheelControl.styling = RotaryWheelStyling(font: fontA, textColour: green, spindleColour: brown, backgroundColour: red, innerGrooveColour: pink, outerGrooveColour: pink)
      wheelContainer.addSubview(wheelControl)
      wheelControl.topAnchor.constraint(equalTo: wheelContainer.topAnchor).isActive = true
      wheelControl.leftAnchor.constraint(equalTo: wheelContainer.leftAnchor).isActive = true
      wheelControl.rightAnchor.constraint(equalTo: wheelContainer.rightAnchor).isActive = true
      wheelControl.bottomAnchor.constraint(equalTo: wheelContainer.bottomAnchor).isActive = true
    }
  }
  @IBOutlet weak var cover: UIView! {
    didSet {
      let rect = CGRect(x: 20, y: 140, width: 100, height: 70)
      let maskLayer = CAShapeLayer()
      maskLayer.frame = cover.bounds
//      let radius: CGFloat = 50.0
//      let rect = CGRect(x: 100, y: 100, width: 2 * radius, height: 2 * radius)
      //    let circlePath = UIBezierPath(ovalIn: rect)
      let boxPath = UIBezierPath(roundedRect: rect, cornerRadius: 8)
      let path = UIBezierPath(rect: cover.bounds)
      path.append(boxPath)
      maskLayer.fillRule = .evenOdd
      maskLayer.path = path.cgPath
      cover.layer.mask = maskLayer
    }
  }
  @IBAction private func tapGestureRecognizerTop(_ sender: UITapGestureRecognizer) {
    wheelControl.selectNextOptionClockwise()
  }
  @IBAction private func tapGestureRecognizerBottom(_ sender: UITapGestureRecognizer) {
    wheelControl.selectNextOptionAntiClockwise()
  }
}
