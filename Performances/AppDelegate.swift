import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        prepareStorage()
        prepareAppearance()
        return true
    }
    
    private func prepareStorage() {
        let context = Storage.sharedInstance.persistentContainer.viewContext
        try! DataBuilder(context: context).populateDatabase()
        try! context.save()
    }
    
    private func prepareAppearance() {
        UINavigationBar.appearance().tintColor = black
        UINavigationBar.appearance().barTintColor = green
        UINavigationBar.appearance().isTranslucent = false
        UISegmentedControl.appearance().tintColor = black
    }
}
