import UIKit

final class PerformersTableViewController: UIViewController {
    
    private let performersController = PerformerController<PerformerTableView>()
    private let navigationStackHandler = PerformersNavigationStackHandler()
    private(set) lazy var tableViewHandler = PerformerTableViewHandler(performersController)
    
    private let searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.placeholder = "Search Performers"
        controller.searchBar.tintColor = black
        controller.searchBar.scopeButtonTitles = ["Any", Performer.Gender.male.description.capitalized, Performer.Gender.female.description.capitalized]
        return controller
    }()
    
    @IBOutlet weak var tableView: PerformerTableView! {
        didSet {
            tableView.dataSource = tableViewHandler
            tableView.delegate = tableViewHandler
            var insets = tableView.contentInset
            insets.top += 5
            insets.bottom += 5
            tableView.contentInset = insets
        }
    }
    
    override var navigationItem: UINavigationItem {
        let item = super.navigationItem
        item.searchController = searchController
        item.hidesSearchBarWhenScrolling = false
        return item
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Performers"
        
        navigationController?.delegate = navigationStackHandler
        
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        
        definesPresentationContext = true
        
        performersController.delegate = tableView
        
        tableViewHandler.didSelect = { [unowned self] performer in
            self.performSegue(withIdentifier: "Show", sender: performer)
        }
        
        do {
            try performersController.reload()
        } catch {
            fatalError(message1)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = tableViewHandler.deselectSelected() {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as? PerformerViewController)?.performer = sender as? Performer
    }
}

extension PerformersTableViewController: UISearchBarDelegate, UISearchResultsUpdating {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let text = ""
        searchBar.text = text
        let scope: Int = 0
        searchBar.selectedScopeButtonIndex = scope
        filterContentForSearchText(text, scope: scope)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let text = searchBar.text.require()
        filterContentForSearchText(text, scope: selectedScope)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let text = searchBar.text.require()
        let selectedScope = searchBar.selectedScopeButtonIndex
        filterContentForSearchText(text, scope: selectedScope)
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: Int) {
        let titles = searchController.searchBar.scopeButtonTitles.require()
        let title = titles[scope]
        if let gender = Performer.Gender(rawValue: title.lowercased()) {
            performersController.updatePredicate(fullName: searchText, gender: gender)
        } else {
            performersController.updatePredicate(fullName: searchText)
        }
    }
}
