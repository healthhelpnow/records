import UIKit

final class DummyProfileImageView: UIView {
    
    init(frame: CGRect, _ profileImageView: UIView) {
        super.init(frame: frame)
        layer.masksToBounds = profileImageView.layer.masksToBounds
        layer.cornerRadius = profileImageView.layer.cornerRadius
        backgroundColor = profileImageView.backgroundColor.require()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
