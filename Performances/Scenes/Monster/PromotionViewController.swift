import UIKit

final class PromotionViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard messageLabel != nil else { return }
        let interval: TimeInterval = 0.6
        UIView.animate(withDuration: interval, animations: {
            self.messageLabel.isHidden = false
        }) { (_) in
            dispatchAfter(seconds: 1, block: {
                self.swapRoot("Main")
            })
        }
        UIView.animate(withDuration: interval / 2.0, delay: interval / 2.0, options: [], animations: {
            self.messageLabel.alpha = 1
        }, completion: nil)
    }
    
    private func swapRoot(_ storyboardName: String) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = (storyboard.instantiateInitialViewController() as? TabBarController).require()
        _ = controller.view.snapshotView(afterScreenUpdates: true) // layout all the things
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate).require()
        let window = appDelegate.window.require()
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: {
          window.rootViewController = controller
        }, completion: nil)
    }
}
