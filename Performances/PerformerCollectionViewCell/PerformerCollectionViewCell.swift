import UIKit
import Dequable

final class PerformerCollectionViewCell: UICollectionViewCell, DequeableComponentIdentifiable {
    @IBOutlet weak var performerTitleLabel: UILabel!
    @IBOutlet weak var profileBodyLabel: UILabel!
}
