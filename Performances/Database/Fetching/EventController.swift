import Records

final class EventController<D: FetchedResultsControllerDelegate>: FetchedResultsController<D> where D.Entity: Event {
    
    convenience init() {
        do {
            try self.init(context: Storage.sharedInstance.persistentContainer.viewContext)
        } catch {
            fatalError(message1)
        }
    }
    
    override func sortDescriptors() -> [NSSortDescriptor] {
        return [NSSortDescriptor(key: "startDate", ascending: true)]
    }
}
