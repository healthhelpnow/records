import UIKit
import Dequable
import Records

final class PerformersCollectionView: UICollectionView, DequeableCollectionView {
    
    func dequeue(at indexPath: IndexPath, for entity: Performer) -> PerformerCollectionViewCell {
        let configurator = PerformerCollectionViewCellConfigurator<PerformerViewModel>(
            titleKeyPath: \.title,
            subtitleKeyPath: \.subtitle
        )
        let cell: PerformerCollectionViewCell = dequeue(indexPath)
        configurator.configure(cell: cell, model: .init(entity))
        return cell
    }
}

extension PerformersCollectionView: FetchedResultsControllerDelegate {
    
    func updateCell(at indexPath: IndexPath, for entity: Performer) {
        _ = dequeue(at: indexPath, for: entity)
    }
}
