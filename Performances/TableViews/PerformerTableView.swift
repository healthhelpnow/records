import UIKit
import Dequable
import Records

class PerformerTableView: UITableView, DequeableTableView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        register(cellType: PerformerTableViewCell.self, hasXib: true)
        backgroundColor = lightGray
    }
    
    func dequeue(at indexPath: IndexPath, for entity: Performer) -> PerformerTableViewCell {
        let configurator = PerformerTableViewCellConfigurator<PerformerViewModel>(
            titleKeyPath: \.title,
            subtitleKeyPath: \.subtitle
        )
        let cell: PerformerTableViewCell = dequeue(indexPath)
        configurator.configure(cell: cell, model: .init(entity))
        return cell
    }
}

extension PerformerTableView: FetchedResultsControllerDelegate {
    
    typealias Entity = Performer
    
    func updateCell(at indexPath: IndexPath, for entity: PerformerTableView.Entity) {
        _ = dequeue(at: indexPath, for: entity)
    }
}
