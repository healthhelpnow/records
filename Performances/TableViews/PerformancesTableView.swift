import UIKit
import Dequable
import Records

class PerformancesTableView: UITableView, DequeableTableView {
    
    typealias Special = () -> Set<Performer>?
    
    var highlightedPerformers: Special?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        register(cellType: PerformanceTableViewCell.self)
    }
    
    func dequeue(at indexPath: IndexPath, for entity: Performance) -> PerformanceTableViewCell {
        let configurator = PerformanceTableViewCellConfigurator<PerformanceViewModel>(
            titleKeyPath: \.title,
            subtitleKeyPath: \.subtitle
        )
        let performers = highlightedPerformers?()
        let cell: PerformanceTableViewCell = dequeue(indexPath)
        configurator.configure(cell: cell, model: .init(entity, performers))
        return cell
    }
}

extension PerformancesTableView: FetchedResultsControllerDelegate {
    
    typealias Entity = Performance
    
    func updateCell(at indexPath: IndexPath, for entity: PerformancesTableView.Entity) {
        _ = dequeue(at: indexPath, for: entity)
    }
}
