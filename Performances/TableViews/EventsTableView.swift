import UIKit
import Dequable
import Records

final class EventsTableView: UITableView, DequeableTableView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        register(cellType: EventTableViewCell.self)
    }
    
    func dequeue(at indexPath: IndexPath, for entity: Event) -> EventTableViewCell {
        let configurator = EventTableViewCellConfigurator<EventViewModel>(
            titleKeyPath: \.title,
            subtitleKeyPath: \.subtitle,
            accessoryTypeKeyPath: \.accessoryType
        )
        let cell: EventTableViewCell = dequeue(indexPath)
        configurator.configure(cell: cell, model: .init(entity))
        return cell
    }
}

extension EventsTableView: FetchedResultsControllerDelegate {
    
    typealias Entity = Event
    
    func updateCell(at indexPath: IndexPath, for entity: EventsTableView.Entity) {
        _ = dequeue(at: indexPath, for: entity)
    }
}
