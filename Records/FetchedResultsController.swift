import UIKit
import CoreData

open class FetchedResultsController<Delegate: FetchedResultsControllerDelegate>: NSObject, NSFetchedResultsControllerDelegate {
    
    public typealias ContentChanged = (Int) -> Void
    
    public var contentChanged: ContentChanged?
    
    public weak var delegate: Delegate?
    
    public let context: NSManagedObjectContext
    
    public private(set) lazy var fetchedResultsController: NSFetchedResultsController<Delegate.Entity> = {
        return build()
    }()
    
    public init(context: NSManagedObjectContext) throws {
        self.context = context
        super.init()
    }
    
    private func load() throws {
        fetchedResultsController = build()
        try fetchedResultsController.performFetch()
    }
    
    public func reload() throws {
        try load()
        delegate?.didReload()
    }
    
    private func build() -> NSFetchedResultsController<Delegate.Entity> {
        let name = typeName(Delegate.Entity.self)
        let fetchRequest: NSFetchRequest<Delegate.Entity> = NSFetchRequest<Delegate.Entity>(entityName: name)
        fetchRequest.fetchBatchSize = fetchBatchSize
        fetchRequest.fetchLimit = fetchLimit
        fetchRequest.predicate = self.predicate()
        fetchRequest.sortDescriptors = self.sortDescriptors()
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.context, sectionNameKeyPath: sectionNameKeyPath(), cacheName: nil)
        controller.delegate = self
        return controller
    }
    
    open var fetchLimit: Int {
        return 0
    }
    
    open var fetchBatchSize: Int {
        return 100
    }
    
    open func predicate() -> NSCompoundPredicate { return NSCompoundPredicate(andPredicateWithSubpredicates: []) }
    
    open func sortDescriptors() -> [NSSortDescriptor] { return [] }
    
    open func sectionNameKeyPath() -> String? { return nil }
    
    //MARK: NSFetchedResultsControllerDelegate
    
    private var tasks = [FetchedResultsControllerTask<Delegate.Entity>]()
    
    open func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tasks.removeAll()
    }
    
    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if newIndexPath != nil {
                insertRowsAt(indexPaths: [newIndexPath!])
            }
        case .delete:
            if indexPath != nil {
                deleteRowsAt(indexPaths: [indexPath!])
            }
        case .update:
            if let entity = anObject as? Delegate.Entity, indexPath != nil {
                update(at: indexPath!, with: entity)
            }
        case .move:
            if indexPath != nil {
                deleteRowsAt(indexPaths: [indexPath!])
            }
            if newIndexPath != nil {
                insertRowsAt(indexPaths: [newIndexPath!])
            }
        }
    }
    
    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        if case .delete = type {
            deleteSectionAt(section: sectionIndex)
        }
        if case .insert = type {
            insertSectionAt(section: sectionIndex)
        }
    }
    
    private func insertRowsAt(indexPaths: [IndexPath]) {
        let task = FetchedResultsControllerTask<Delegate.Entity>.insertRowsAt(indexPaths: indexPaths)
        tasks.append(task)
    }
    
    private func insertSectionAt(section: Int) {
        let task = FetchedResultsControllerTask<Delegate.Entity>.insertSectionAt(section: section)
        tasks.append(task)
    }
    
    private func deleteRowsAt(indexPaths: [IndexPath]) {
        let task = FetchedResultsControllerTask<Delegate.Entity>.deleteRowsAt(indexPaths: indexPaths)
        tasks.append(task)
    }
    
    private func deleteSectionAt(section: Int) {
        let task = FetchedResultsControllerTask<Delegate.Entity>.deleteSectionAt(section: section)
        tasks.append(task)
    }
    
    private func update(at indexPath: IndexPath, with entity: Delegate.Entity) {
        let task = FetchedResultsControllerTask<Delegate.Entity>.update(at: indexPath, with: entity)
        tasks.append(task)
    }
    
    open func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.perform(tasks: tasks)
        tasks.removeAll()
        contentChanged?(controller.fetchedObjects?.count ?? 0)
    }
}
