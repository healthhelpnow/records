import Foundation
import CoreData

public enum FetchedResultsControllerTask<Entity: NSManagedObject> {
    case insertRowsAt(indexPaths: [IndexPath])
    case insertSectionAt(section: Int)
    case deleteRowsAt(indexPaths: [IndexPath])
    case deleteSectionAt(section: Int)
    case update(at: IndexPath, with: Entity)
}
