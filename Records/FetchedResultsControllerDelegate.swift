import UIKit
import CoreData

public protocol FetchedResultsControllerDelegate: class {
    associatedtype Entity: NSManagedObject
    func updateCell(at indexPath: IndexPath, for entity: Entity)
    func perform(tasks: [FetchedResultsControllerTask<Entity>])
    func didReload()
}

public extension FetchedResultsControllerDelegate where Self: UICollectionView {
    
    func perform(tasks: [FetchedResultsControllerTask<Entity>]) {
        performBatchUpdates({
            go(tasks)
        }, completion: nil)
    }
    
    private func go(_ tasks: [FetchedResultsControllerTask<Entity>]) {
        for task in tasks {
            switch task {
            case .deleteRowsAt(indexPaths: let indexPaths):
                deleteItems(at: indexPaths)
            case .deleteSectionAt(section: let section):
                deleteSections(IndexSet(integer: section))
            case .insertRowsAt(indexPaths: let indexPaths):
                insertItems(at: indexPaths)
            case .insertSectionAt(section: let section):
                insertSections(IndexSet(integer: section))
            case .update(at: let indexPath, with: let entity):
                updateCell(at: indexPath, for: entity)
            }
        }
    }
    
    func didReload() {
        reloadData()
    }
}

public extension FetchedResultsControllerDelegate where Self: UITableView {
    
    func perform(tasks: [FetchedResultsControllerTask<Entity>]) {
        beginUpdates()
        go(tasks)
        endUpdates()
    }
    
    private func go(_ tasks: [FetchedResultsControllerTask<Entity>]) {
        for task in tasks {
            switch task {
            case .deleteRowsAt(indexPaths: let indexPaths):
                deleteRows(at: indexPaths, with: .fade)
            case .deleteSectionAt(section: let section):
                deleteSections(IndexSet(integer: section), with: .automatic)
            case .insertRowsAt(indexPaths: let indexPaths):
                insertRows(at: indexPaths, with: .fade)
            case .insertSectionAt(section: let section):
                insertSections(IndexSet(integer: section), with: .automatic)
            case .update(at: let indexPath, with: let entity):
                updateCell(at: indexPath, for: entity)
            }
        }
    }
    
    func didReload() {
        reloadData()
    }
}
