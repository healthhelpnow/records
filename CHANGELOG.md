# Change Log

## [7.1.1](https://github.com/rob-nash/Records/releases/tag/7.1.1) (2019-Feb-28)

 - Added sort descriptor param to 'fetchAll()'. Ref: [Issue 18](https://github.com/rob-nash/Records/issues/18).

## [7.1.0](https://github.com/rob-nash/Records/releases/tag/7.1.0) (2019-Feb-28)

 - Fetch limit parameter added to 'fetchAll()' function with default value. Ref: [Issue 16](https://github.com/rob-nash/Records/issues/16).

## [7.0.0](https://github.com/rob-nash/Records/releases/tag/7.0.0) (2019-Feb-16)

 - Dates are now correctly queried with ranges, not equality.

## [6.2.2](https://github.com/rob-nash/Records/releases/tag/6.2.2) (2019-Feb-15)

 - Fixed [Issue 12](https://github.com/rob-nash/Records/issues/12#issue-410689089)

## [6.2.1](https://github.com/rob-nash/Records/releases/tag/6.2.1) (2019-Feb-02)

 - Designated initialiser in FetchedResultsController no longer a required initialiser. This is simply not necessary and it started to get in the way.

## [6.2.0](https://github.com/rob-nash/Records/releases/tag/6.2.0) (2019-Feb-01)

 - FetchedResultsController API opened for the following.

```swift
open var fetchLimit: Int
open var fetchBatchSize: Int
```

[Apple Docs](https://developer.apple.com/documentation/coredata/nsfetchrequest/1506622-fetchlimit?language=objc)
[iOS SDK bug report](http://www.openradar.me/30381905).

## [6.1.0](https://github.com/rob-nash/Records/releases/tag/6.1.0) (2019-Jan-24)

 - Int64 attributes now supported.

## [6.0.0](https://github.com/rob-nash/Records/releases/tag/6.0.0) (2018-Nov-16)

 - There is no longer a requirement to set a datasource on your FetchedResultsController object.
 - A unique UITableView or UICollectionView subclass must now be created for each Entity you want to observe.
 - The FetchedResultsControllerDelegate is now responsible for updating cells and you are expecting to implement this method in your UITableView or UICollectionView subclass. See README.md for more info.

## [5.0.0](https://github.com/rob-nash/Records/releases/tag/5.0.0) (2018-Nov-10)

 - FetchedResultsController now supports UICollectionView

## [4.0.0](https://github.com/rob-nash/Records/releases/tag/4.0.0) (2018-Sep-17)

 - Swift 4.2

## [3.2.3](https://github.com/rob-nash/Records/releases/tag/3.2.3) (2018-Jun-07)

## [3.2.2](https://github.com/rob-nash/Records/releases/tag/3.2.2) (2018-May-26)

 - No longer sharing unnecessary scheme

## [3.2.1](https://github.com/rob-nash/Records/releases/tag/3.2.1) (2018-May-26)

 - Bug fix

## [3.2.0](https://github.com/rob-nash/Records/releases/tag/3.2.0) (2018-May-25)

 - Create unique records from datasets conforming to Recordable.

## [3.1.2](https://github.com/rob-nash/Records/releases/tag/3.1.2) (2018-May-02)

## [3.1.1](https://github.com/rob-nash/Records/releases/tag/3.1.1) (2018-Apr-30)

 - Sorcery config file is one line smaller :)

## [3.1.0](https://github.com/rob-nash/Records/releases/tag/3.1.0) (2018-Apr-18)

 - Some refactoring that brings about the deprecation of RelationshipRestriction.

## [3.0.0](https://github.com/rob-nash/Records/releases/tag/3.0.0) (2018-Apr-18)

 - The default template for Sourcery has changed to provide default values of nil, for each query parameter.

## [2.0.0](https://github.com/rob-nash/Records/releases/tag/2.0.0) (2018-Mar-04)

## [1.0.5](https://github.com/rob-nash/Records/releases/tag/1.0.5) (2018-Jan-29)

 - fetchedObjectsCount is an Int of current count

```swift
fetchedResultsController.contentChanged = { fetchedObjectsCount in }
```

## [1.0.4](https://github.com/rob-nash/Records/releases/tag/1.0.4) (2018-Jan-04)

 - New function added for counting entities.

## [1.0.3](https://github.com/rob-nash/Records/releases/tag/1.0.3) (2017-Dec-18)

 - Removing the demo and uI test logic from this project and moving it elsewhere. Checkouts with this tag will be more lightweight.

## [1.0.2](https://github.com/rob-nash/Records/releases/tag/1.0.2) (2017-Nov-29)

 - Underscore notation extended to all types. Was originally just enums.

## [1.0.1](https://github.com/rob-nash/Records/releases/tag/1.0.1) (2017-Nov-18)

 - Fixed: Sourcery boiler plate generation is writing double optionals if the original type was already optional.

## [1.0.0](https://github.com/rob-nash/Records/releases/tag/1.0.0) (2017-Nov-09)

- First release
